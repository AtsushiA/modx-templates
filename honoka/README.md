# Honoka Theme for MODX

original css style [http://honokak.osaka/](http://honokak.osaka/)

"Honoka" は日本語も美しく表示できるBootstrapテーマです。
をMODX用外部テーマにカスタマイズ中

## About "Honoka"

通常の[Bootstrap](http://getbootstrap.com/)では，日本語のフォント指定や文字サイズは最適とはいえません。"Honoka"はそんなBootstrapをベースに，日本語表示に適したフォント指定や，文字サイズに関するコードを追記したBootstrapテーマの一つです。

## Usage to MODX Theme

### Package Loacation

``main.html``といったように，ファイル名に``min``がつくファイルは，改行やインデント・スペーシングをなくした(minifyされた)コードで，ユーザがウェブページを読み込む際の転送量を少なくすることができます。通常はこの``bootstrap.min.*``を使うことをおすすめします。

```
[MODX Root]/
└─ assets
   └─ tempates
  	     └─honoka
			├── README.md
			├── bootstrap.html (honoka Original Sample)
			├── css
			│   ├── bootstrap.css
			│   └── bootstrap.min.css
			├── fonts
			│   ├── glyphicons-halflings-regular.eot
			│   ├── glyphicons-halflings-regular.svg
			│   ├── glyphicons-halflings-regular.ttf
			│   ├── glyphicons-halflings-regular.woff
			│   └── glyphicons-halflings-regular.woff2
			├── js
			│   ├── bootstrap.js
			│   └── bootstrap.min.js
			└── main.html (Main Templates)
```


ダッシュボードのテンプレート編集にて新規テンプレートを作成しテンプレートコード  
``@FILE:/assets/templates/honoka/main.html``
を指定

メインを親としたコンテンツ要素を追加する場合は``contets_main``  
``contents_2col``のように``contents_``で命名する。


## License

[MIT Licesne](LICENSE)

## Author

 * windyakin ([windyakin.net](http://windyakin.net/))
 
 * MODX Teheme NExT-Season ([next-season.net](http://www.next-season.net))